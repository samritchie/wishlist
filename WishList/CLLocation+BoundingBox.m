//
//  CLLocation+BoundingBox.m
//  WishList
//
//  Created by Sam Ritchie on 8/05/2014.
//  Copyright (c) 2014 Sam Ritchie. All rights reserved.
//

#import "CLLocation+BoundingBox.h"
#import <MapKit/MapKit.h>

@implementation CLLocation (BoundingBox)

- (CBLGeoRect)boundingBoxWithDistance:(double)distance {
    CLLocationCoordinate2D centre = self.coordinate;
    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(centre, distance, distance);
    CBLGeoPoint northEastCorner, southWestCorner;
    northEastCorner.y = centre.latitude + (region.span.latitudeDelta  / 2.0);
    northEastCorner.x = centre.longitude + (region.span.longitudeDelta / 2.0);
    southWestCorner.y = centre.latitude - (region.span.latitudeDelta  / 2.0);
    southWestCorner.x = centre.longitude - (region.span.longitudeDelta / 2.0);
    CBLGeoRect box = { southWestCorner, northEastCorner };
    return box;
}
@end
