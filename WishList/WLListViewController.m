//
//  WLListViewController.m
//  WishList
//
//  Created by Sam Ritchie on 6/05/2014.
//  Copyright (c) 2014 Sam Ritchie. All rights reserved.
//

#import "WLListViewController.h"
#import "WLAppDelegate.h"
#import "WLStore.h"
#import "WLItem.h"

@interface WLListViewController ()
@property (readonly) CBLDatabase *database;
@property (nonatomic) NSDictionary *items;
@end

@implementation WLListViewController
#pragma mark - Managing the detail item

- (void)setOwner:(NSString *)newOwner
{
    if (_owner != newOwner) {
        _owner = newOwner;
        self.title = _owner;
        
        NSMutableDictionary *results = [NSMutableDictionary dictionary];
        CBLQuery *query = [[self.database viewNamed:@"itemsByOwnerAndStore"] createQuery];
        query.startKey = @[_owner];
        query.endKey = @[_owner, @{}];
        query.prefetch = YES;
        query.mapOnly = YES;
        for (CBLQueryRow *row in [[query run:NULL] allObjects]) {
            if (!results[row.key1])
                results[row.key1] = @[];
            results[row.key1] = [results[row.key1] arrayByAddingObject:row.document];
        }
        
        self.items = [results copy];
        [self.tableView reloadData];
    }
}

- (CBLDatabase *)database {
    return [(WLAppDelegate *)[[UIApplication sharedApplication] delegate] database];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table View

- (int)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.items.allKeys.count;
}

- (int)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.items[self.items.allKeys[section]] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSNumberFormatter *currencyFormatter;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        currencyFormatter = [[NSNumberFormatter alloc] init];
        currencyFormatter.numberStyle = NSNumberFormatterCurrencyStyle;
    });
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    NSString *store = self.items.allKeys[indexPath.section];
    CBLDocument *doc = self.items[store][indexPath.row];
    WLItem *item = (WLItem *)[doc modelWithClass:[WLItem class]];
    
    cell.detailTextLabel.text = [currencyFormatter stringFromNumber:@(item.price)];
    if ([item.status isEqualToString:@"available"])
        cell.textLabel.text = item.name;
    else
        cell.textLabel.attributedText = [[NSAttributedString alloc] initWithString:item.name
                                                                        attributes:@{NSStrikethroughStyleAttributeName : [NSNumber numberWithInt:NSUnderlineStyleSingle]}];
    
    return cell;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return self.items.allKeys[section];
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return NO;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    NSString *store = self.items.allKeys[indexPath.section];
    CBLDocument *doc = self.items[store][indexPath.row];
    WLItem *item = (WLItem *)[doc modelWithClass:[WLItem class]];
    
    if ([item.status isEqualToString:WLItemAvailable]) {
        item.status = WLItemUnavailable;
        cell.textLabel.attributedText = [[NSAttributedString alloc]
                                         initWithString:item.name
                                             attributes:@{NSStrikethroughStyleAttributeName : [NSNumber numberWithInt:NSUnderlineStyleSingle]}];
    }
    else {
        item.status = WLItemAvailable;
        cell.textLabel.text = item.name;
    }
    NSError *error;
    if (![doc updateModel:item error:&error])
        NSLog(@"Error saving document: %@", error.localizedDescription);
    
}

@end
