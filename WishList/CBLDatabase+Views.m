//
//  CBLDatabase+Views.m
//  WishList
//
//  Created by Sam Ritchie on 7/05/2014.
//  Copyright (c) 2014 Sam Ritchie. All rights reserved.
//

#import "CBLDatabase+Views.h"

@implementation CBLDatabase (Views)

- (void)registerViews {
    
    [[self viewNamed:@"itemsByOwnerAndStore"] setMapBlock: MAPBLOCK({
        emit(@[doc[@"owner"], doc[@"store"][@"name"]], @1);
    }) reduceBlock: REDUCEBLOCK({
        int sum = 0;
        for (NSNumber *count in values)
            sum += [count intValue];
        return @(sum);
    }) version: @"1.0"];

    [[self viewNamed:@"fullText"] setMapBlock: MAPBLOCK({
        emit(CBLTextKey(doc[@"name"]), nil);
    }) version: @"1.0"];
    
    [[self viewNamed:@"storesByLocation"] setMapBlock: MAPBLOCK({
        NSDictionary *store = doc[@"store"];
        if (![store[@"location"] isKindOfClass:[NSNull class]] && [store[@"location"] count] == 2) {
            id point = CBLGeoPointKey([store[@"location"][1] doubleValue], [store[@"location"][0] doubleValue]);
            emit(point, @{@"store" : store[@"name"], @"owner" : doc[@"owner"]});
        }
    }) version: @"1.0"];
    
    [[self viewNamed:@"conflicts"] setMapBlock: MAPBLOCK({
        if (doc[@"_conflicts"])
            emit(doc[@"_id"], nil);
    }) version: @"1.0"];
}



- (void)resolveConflictsUsingBlock:(void(^)(CBLDocument *document, CBLSavedRevision *conflict, CBLSavedRevision *parent))mergeBlock {
    NSError *error = nil;
    
    CBLQuery *query = [[self viewNamed:@"conflicts"] createQuery];
    CBLQueryEnumerator *enumerator = [query run:&error];
    if (!enumerator)
        NSLog(@"Error running conflict query");

    for (CBLQueryRow *row in [enumerator allObjects]) {
        CBLDocument *doc = row.document;
        NSArray *conflicts = [doc getConflictingRevisions:&error];
        if (!conflicts)
            NSLog(@"Error retrieving revisions: %@", error.localizedDescription);
        
        NSString *currentRevision = doc.currentRevisionID;
        for (CBLSavedRevision *conflict in conflicts) {
            if ([conflict.revisionID isEqualToString:currentRevision])
                continue;
            CBLSavedRevision *parent = [doc commonParent:conflict];
            mergeBlock(doc, conflict, parent);
        }
    }
}


@end
