//
//  WLListViewController.h
//  WishList
//
//  Created by Sam Ritchie on 6/05/2014.
//  Copyright (c) 2014 Sam Ritchie. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WLListViewController : UITableViewController

@property (strong, nonatomic) NSString *owner;

@end
