//
//  CBLDatabase+Views.h
//  WishList
//
//  Created by Sam Ritchie on 7/05/2014.
//  Copyright (c) 2014 Sam Ritchie. All rights reserved.
//

#import <CouchbaseLite/CouchbaseLite.h>

@interface CBLDatabase (Views)

- (void)registerViews;


- (void)resolveConflictsUsingBlock:(void(^)(CBLDocument *document, CBLSavedRevision *conflict, CBLSavedRevision *parent))mergeBlock;

@end
