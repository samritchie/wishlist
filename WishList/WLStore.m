//
//  WLStore.m
//  WishList
//
//  Created by Sam Ritchie on 8/05/2014.
//  Copyright (c) 2014 Sam Ritchie. All rights reserved.
//

#import "WLStore.h"

@implementation WLStore

- (instancetype)initWithJSON:(NSDictionary *)jsonObject {
    self = [super init];
    if (self) {
        self.name = ValueOrNil(jsonObject[@"name"]);
        self.website = ValueOrNil(jsonObject[@"website"]);
        self.location = ValueOrNil(jsonObject[@"location"]);
    }
    return self;
}

- (NSDictionary *)toJSON {
    return @{@"name": ValueOrNSNull(self.name),
             @"website": ValueOrNSNull(self.website),
             @"location": ValueOrNSNull(self.location)};
}

@end
