//
//  WLStore.h
//  WishList
//
//  Created by Sam Ritchie on 8/05/2014.
//  Copyright (c) 2014 Sam Ritchie. All rights reserved.
//

#import <Foundation/Foundation.h>
@class WLItem;

@interface WLStore : NSObject<WLJSONSerialisation>

@property (nonatomic) NSString *name;
@property (nonatomic) NSArray *location;
@property (nonatomic) NSURL *website;

@end
