//
//  WLItem.m
//  WishList
//
//  Created by Sam Ritchie on 8/05/2014.
//  Copyright (c) 2014 Sam Ritchie. All rights reserved.
//

#import "WLItem.h"
#import "WLStore.h"

NSString *const WLItemAvailable = @"available";
NSString *const WLItemUnavailable = @"unavailable";

@implementation WLItem
- (instancetype)initWithJSON:(NSDictionary *)jsonObject {
    self = [super init];
    if (self) {
        self.revision = ValueOrNil(jsonObject[@"_rev"]);
        self.name = ValueOrNil(jsonObject[@"name"]);
        self.owner = ValueOrNil(jsonObject[@"owner"]);
        self.store = ValueOrNil([[WLStore alloc] initWithJSON:jsonObject[@"store"]]);
        self.type = ValueOrNil(jsonObject[@"type"]);
        self.price = [jsonObject[@"price"] doubleValue];
        self.status = ValueOrNil(jsonObject[@"status"]);
    }
    return self;
}

- (NSDictionary *)toJSON {
    return @{@"_rev": ValueOrNSNull(self.revision),
             @"name": ValueOrNSNull(self.name),
             @"owner": ValueOrNSNull(self.owner),
             @"store": ValueOrNSNull([self.store toJSON]),
             @"type": ValueOrNSNull(self.type),
             @"status": ValueOrNSNull(self.status),
             @"price": [NSNumber numberWithDouble:self.price]
             };
}

- (BOOL)isAvailable {
    return [self.status isEqualToString:WLItemAvailable];
}

- (void)setIsAvailable:(BOOL)isAvailable {
    if (isAvailable)
        self.status = WLItemAvailable;
    else
        self.status = WLItemUnavailable;
}

- (void)merge:(WLItem *)other withOriginal:(WLItem *)original {
    if (original.isAvailable && !self.isAvailable && !other.isAvailable)
        NSLog(@"Oh Noes! 2 items have been purchased");
    else if (original.isAvailable && (!self.isAvailable || !other.isAvailable))
        self.isAvailable = NO;
    else if (!original.isAvailable && (self.isAvailable || other.isAvailable))
        self.isAvailable = YES;

}
@end
