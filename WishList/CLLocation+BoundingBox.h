//
//  CLLocation+BoundingBox.h
//  WishList
//
//  Created by Sam Ritchie on 8/05/2014.
//  Copyright (c) 2014 Sam Ritchie. All rights reserved.
//

#import <CoreLocation/CoreLocation.h>

@interface CLLocation (BoundingBox)
- (CBLGeoRect)boundingBoxWithDistance:(double)distance;
@end
