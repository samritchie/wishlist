//
//  WLJSONSerialisation.h
//  WishList
//
//  Created by Sam Ritchie on 8/05/2014.
//  Copyright (c) 2014 Sam Ritchie. All rights reserved.
//

#import <Foundation/Foundation.h>

#define ValueOrNSNull(obj) (obj ? obj : [NSNull null])
#define ValueOrNil(obj) ([obj isEqual:[NSNull null]] ? nil : obj)

@protocol WLJSONSerialisation <NSObject>

- (instancetype)initWithJSON:(NSDictionary *)jsonObject;
- (NSDictionary *)toJSON;

@optional
@property (nonatomic) NSString *revision;
@end
