//
//  WLMasterViewController.h
//  WishList
//
//  Created by Sam Ritchie on 6/05/2014.
//  Copyright (c) 2014 Sam Ritchie. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WLMasterViewController : UITableViewController<UISearchBarDelegate, UISearchDisplayDelegate>
- (IBAction)locationButtonTapped:(id)sender;
- (IBAction)syncButtonTapped:(id)sender;

@end
