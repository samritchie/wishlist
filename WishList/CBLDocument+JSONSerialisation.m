//
//  CBLDocument+JSONSerialisation.m
//  WishList
//
//  Created by Sam Ritchie on 8/05/2014.
//  Copyright (c) 2014 Sam Ritchie. All rights reserved.
//

#import "CBLDocument+JSONSerialisation.h"
#import "objc/runtime.h"

@implementation CBLDocument (JSONSerialisation)

- (id)modelWithClass:(Class)cls {
    return [[cls alloc] initWithJSON:self.properties];
}

- (BOOL)updateModel:(id<WLJSONSerialisation>)model error:(NSError **)error {
    CBLSavedRevision *newRev = [self putProperties:[model toJSON] error:error];
    if ([model respondsToSelector:@selector(setRevision:)] && newRev)
        [model setRevision:newRev.revisionID];
    return (newRev != nil);
}


- (CBLSavedRevision *)commonParent:(CBLSavedRevision *)conflict {
    NSArray *currentRevisionHistory = [self getRevisionHistory:NULL];
    NSArray *conflictRevisionHistory = [conflict getRevisionHistory:NULL];
    CBLSavedRevision *parent = nil;
    for (int i = 0; i < MAX(currentRevisionHistory.count, conflictRevisionHistory.count); i++) {
        if ([[currentRevisionHistory[i] revisionID] isEqualToString:[conflictRevisionHistory[i] revisionID]])
            parent = currentRevisionHistory[i];
        else
            return parent;
    }
    return parent;
}


@end
