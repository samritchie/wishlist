//
//  WLAppDelegate.h
//  WishList
//
//  Created by Sam Ritchie on 6/05/2014.
//  Copyright (c) 2014 Sam Ritchie. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WLAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) CBLDatabase *database;
@property (strong, nonatomic) CBLReplication *pull;
@property (strong, nonatomic) CBLReplication *push;

@end
