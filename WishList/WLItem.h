//
//  WLItem.h
//  WishList
//
//  Created by Sam Ritchie on 8/05/2014.
//  Copyright (c) 2014 Sam Ritchie. All rights reserved.
//

#import <Foundation/Foundation.h>

@class WLStore;

extern NSString *const WLItemAvailable;
extern NSString *const WLItemUnavailable;

@interface WLItem : NSObject<WLJSONSerialisation>

@property (nonatomic) NSString *revision;
@property (nonatomic) NSString *name;
@property (nonatomic) NSString *owner;
@property (nonatomic) WLStore *store;
@property (nonatomic) NSString *type;
@property (nonatomic) double price;
@property (nonatomic) NSString *status;
@property (nonatomic) BOOL isAvailable;

- (void)merge:(WLItem *)other withOriginal:(WLItem *)original;

@end
