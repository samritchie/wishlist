//
//  CBLDocument+JSONSerialisation.h
//  WishList
//
//  Created by Sam Ritchie on 8/05/2014.
//  Copyright (c) 2014 Sam Ritchie. All rights reserved.
//

#import <CouchbaseLite/CouchbaseLite.h>
#import "WLJSONSerialisation.h"

@interface CBLDocument (JSONSerialisation)

- (id)modelWithClass:(Class)cls;
- (BOOL)updateModel:(id<WLJSONSerialisation>)model error:(NSError **)error;
- (CBLSavedRevision *)commonParent:(CBLSavedRevision *)conflict;
@end
