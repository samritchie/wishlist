//
//  WLSpatialViewController.m
//  WishList
//
//  Created by Sam Ritchie on 10/05/2014.
//  Copyright (c) 2014 Sam Ritchie. All rights reserved.
//

#import "WLSpatialViewController.h"
#import "WLListViewController.h"

@interface WLSpatialViewController ()
@property NSDictionary *stores;
@end

@implementation WLSpatialViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSMutableDictionary *results = [NSMutableDictionary dictionary];
    for (CBLQueryRow *row in self.enumerator.allObjects) {
        NSString *store = row.value[@"store"];
        NSString *owner = row.value[@"owner"];
        if (!results[store])
            results[store] = @[];
        if (![results[store] containsObject:owner])
            results[store] = [results[store] arrayByAddingObject:owner];
    }
    self.stores = [results copy];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (int)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.stores.allKeys.count;
}

- (int)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.stores[self.stores.allKeys[section]] count];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return self.stores.allKeys[section];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"LocationCell"];
    
    NSString *store = self.stores.allKeys[indexPath.section];
    NSString *owner = self.stores[store][indexPath.row];
    cell.textLabel.text = owner;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.tableView != tableView) {
        [self performSegueWithIdentifier:@"showList" sender:self];
    }
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return NO;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"showList"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        NSString *store = self.stores.allKeys[indexPath.section];
        NSString *owner = self.stores[store][indexPath.row];
       [[segue destinationViewController] setOwner:owner];
    }
}

@end
