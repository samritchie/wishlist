//
//  WLSpatialViewController.h
//  WishList
//
//  Created by Sam Ritchie on 10/05/2014.
//  Copyright (c) 2014 Sam Ritchie. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WLSpatialViewController : UITableViewController

@property (nonatomic) CBLQueryEnumerator *enumerator;

@end
