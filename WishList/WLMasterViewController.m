//
//  WLMasterViewController.m
//  WishList
//
//  Created by Sam Ritchie on 6/05/2014.
//  Copyright (c) 2014 Sam Ritchie. All rights reserved.
//

#import "WLMasterViewController.h"
#import "WLListViewController.h"
#import "WLSpatialViewController.h"
#import "WLAppDelegate.h"
#import <CoreLocation/CoreLocation.h>

@interface WLMasterViewController ()<CLLocationManagerDelegate>
@property (readonly) CBLDatabase *database;
@property () CBLQueryEnumerator *enumerator;
@property () CBLQueryEnumerator *searchResultsEnumerator;
@property () CLLocationManager *locationManager;
@end

@implementation WLMasterViewController

- (void)awakeFromNib
{
    [super awakeFromNib];
}

- (void)viewDidLoad
{
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters;
    self.locationManager.distanceFilter = 500;

    [super viewDidLoad];
    [self showAll];
}

- (CBLDatabase *)database {
    return [(WLAppDelegate *)[[UIApplication sharedApplication] delegate] database];
}

- (void)showAll {
    CBLQuery *query = [[self.database viewNamed:@"itemsByOwnerAndStore"] createQuery];
    query.groupLevel = 1;
    self.enumerator = [query run:NULL];
    [self.tableView reloadData];
}

#pragma mark - Table View

- (int)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (int)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (tableView == self.tableView)
        return self.enumerator.count;
    else
        return self.searchResultsEnumerator.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = nil;

    if (tableView == self.tableView) {
        cell = [self.tableView dequeueReusableCellWithIdentifier:@"Cell"];
        CBLQueryRow *row = [self.enumerator rowAtIndex:indexPath.row];
        cell.textLabel.text = row.key0;
    } else {
        cell = [self.tableView dequeueReusableCellWithIdentifier:@"SearchResultCell"];
        CBLFullTextQueryRow *row = (CBLFullTextQueryRow *)[self.searchResultsEnumerator rowAtIndex:indexPath.row];
        cell.textLabel.text = row.fullText;
        cell.detailTextLabel.text = [NSString stringWithFormat:@"in list '%@'", row.document.properties[@"owner"]];
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.tableView != tableView) {
        [self performSegueWithIdentifier:@"showList" sender:self];
    }
}


- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return NO;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"showList"]) {
        NSString *owner = nil;
        if (self.searchDisplayController.active) {
            NSIndexPath *indexPath = [self.searchDisplayController.searchResultsTableView indexPathForSelectedRow];
            owner = [self.searchResultsEnumerator rowAtIndex:indexPath.row].document.properties[@"owner"];
        } else {
            NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
            owner = [self.enumerator rowAtIndex:indexPath.row].key0;
        }
        [[segue destinationViewController] setOwner:owner];
    }
    if ([[segue identifier] isEqualToString:@"showSpatial"]) {
        [[segue destinationViewController] setEnumerator:sender];
    }
}

#pragma mark location
- (IBAction)locationButtonTapped:(id)sender {
    [self.locationManager startUpdatingLocation];
}


- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    [self.locationManager stopUpdatingLocation];
    CLLocation* location = [locations lastObject];
    
    CBLQuery *query = [[self.database viewNamed:@"storesByLocation"] createQuery];
    query.boundingBox = [location boundingBoxWithDistance:2000.0];
    CBLQueryEnumerator *enumerator = [query run:NULL];
    [self performSegueWithIdentifier:@"showSpatial" sender:enumerator];
}

#pragma mark search

- (void)performSearch:(NSString *)searchText {
    CBLQuery *query = [[self.database viewNamed:@"fullText"] createQuery];
    query.fullTextQuery = [searchText stringByAppendingString:@"*"];
    self.searchResultsEnumerator = [query run:NULL];
    [self.tableView reloadData];
}

- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString {
    if (searchString && searchString.length > 0)
        [self performSearch:searchString];
    else
        self.searchResultsEnumerator = nil;

    return YES;
}

- (void)searchBarCancelButtonClicked:(UISearchBar *) searchBar {
    self.searchResultsEnumerator = nil;
}

#pragma mark sync
- (IBAction)syncButtonTapped:(id)sender {
    [[(WLAppDelegate *)[[UIApplication sharedApplication] delegate] pull] start];
}

@end
