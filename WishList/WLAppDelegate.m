//
//  WLAppDelegate.m
//  WishList
//
//  Created by Sam Ritchie on 6/05/2014.
//  Copyright (c) 2014 Sam Ritchie. All rights reserved.
//

#import "WLAppDelegate.h"
#import "WLItem.h"

NSString *const remoteURL = @"https://<username>:<password>@<couchdbserver>/<database>";

@implementation WLAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    CBLManager *manager = [CBLManager sharedInstance];
    self.database = [manager databaseNamed:@"wishlist" error:NULL];
    if (self.database.documentCount == 0)
        [self rebuildDatabase];
    
    [self.database registerViews];

    NSURL *url = [NSURL URLWithString:remoteURL];
    self.pull = [self.database createPullReplication:url];
    self.push = [self.database createPushReplication:url];
    
    [[NSNotificationCenter defaultCenter] addObserverForName:kCBLReplicationChangeNotification object:self.pull queue:nil usingBlock:^(NSNotification *note) {
        if (!self.pull.running) {
            [self.database resolveConflictsUsingBlock:^(CBLDocument *document, CBLSavedRevision *conflict, CBLSavedRevision *parent) {
                WLItem *item = [[WLItem alloc] initWithJSON:document.properties];
                WLItem *other = [[WLItem alloc] initWithJSON:conflict.properties];
                WLItem *original = [[WLItem alloc] initWithJSON:parent.properties];
                [item merge:other withOriginal:original];
                [document updateModel:item error:NULL];
                [conflict deleteDocument:NULL];
            }];
            [[self push] start];
        }
    }];
    [[self pull] start];
    return YES;
}
							
- (void)rebuildDatabase {
    CBLManager *manager = [CBLManager sharedInstance];
    [self.database deleteDatabase:NULL];
    self.database = [manager databaseNamed:@"wishlist" error:NULL];
    
    NSArray *testData = [NSJSONSerialization JSONObjectWithData:[NSData dataWithContentsOfURL:[[NSBundle mainBundle] URLForResource:@"data" withExtension:@"json"]]
                                                        options:0
                                                          error:NULL];
    for (NSDictionary *testDoc in testData)
        [[self.database createDocument] putProperties:testDoc error:NULL];
}

@end
