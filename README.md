##WishList

This is a Couchbase lite demo app used as part of my [YOW! West presentation](https://a.confui.com/-tODOP700).

You'll need a CouchDB server to run the replication component - you can sign up for a free plan at 
[Cloudant](https://cloudant.com).